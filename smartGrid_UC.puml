@startuml

' ========CONSTANTS
!define NSS NSS
!define SERVICE Service
!define SLICE Slice
!define VERTICAL Vertical

' ===PREPROCESS -- ALLOW SELECTING ONLY SOME STEPS TO SHOW    ====================
!define SHOW_ASSUMPTIONS
!define SHOW_DATA

!define STEP_A
!ifndef TO_A
	!define STEP_B
	!ifndef TO_B
		!define STEP_C
		!ifndef TO_C
			!define STEP_D
			!ifndef TO_D
				!define STEP_E
			!endif
		!endif
	!endif
!endif
!define DETAIL

' ====TITLE========================================================================
title 
=Smart Grid UC
==Story 1
----
Reliable cross NSP SLICE provided by DSP
----
end title

' ====BEGIN========================================================================
start

' ================================================================================
!ifdef STEP_A
:<color:red>A</color>1. **Create NSP offer**
----
NSSs with reliability
[["./smartGrid_UC_step1.svg"]]
;
!ifdef SHOW_DATA
note right
	//**NSS_Offer**(location, time, reliability score)//
	----
	available locations: //<l_1, l_2, ..., l_n//
	available times: //date range//
	available reliability: up to //r//
	----
	Reliability is defined per location of NSS and per time
	Reliability is a probability "score" estimating the chance of success
end note
!endif
!ifdef SHOW_ASSUMPTIONS
floating note left
	**Assumptions**
	----
	* NSP is willing to share some reliability information with DSP
	* NSP can generate per-location reliability scores
	** Collects enough data
	** Analyzes data to produce scores
	*  <color:blue>//I can provide an NSS at location X with reliability guarantee Y(t)//</color>
	** //Y// is reliability (0% to 100%) that is a function of time //t//
	** //Y// is not a function of usage; e.g., does not depend on traffic 
end note
!endif
->Publish to NSP catalogue;
!endif


' ================================================================================
!ifdef STEP_A
:<color:red>A</color>2. **Create DSP offer**
----
An end-to-end SERVICE with reliability
;
!ifdef SHOW_DATA
note right
	//**SERVICE_Offer**(location, time, reliability score)//
	----
	available geography (covered locations): //geo_1, geo_2, ..., geo_m//
	available times: //date range//
	available reliability: up to //r//
	----
	Reliability is defined //per SERVICE// --
	it is a combination of the reliability provided by the NSSs
	which are used to implement the SERVICE SLICE
	and by the guarnatees on the reliability of the composition
end note
!endif
!ifdef SHOW_ASSUMPTIONS
note left
	**Assumptions**
	----
	* DSP can collect and store enough information from potential NSPs
	** Collects reliability per location and time
	** Analyzes data to determine which offerings can be made,
		what locations are covered and at what schedule
	** Q: where is this stored? 
	* DSP can compose a reliable end-to-end service from the available NSPs
	** Can choose, mix, and match NSSs to achieve desired end-to-end goals
	** Can combine NSSs without adding reliability degrdation beyond that of the NSSs
	*  <color:blue>//I can provide an end-to-end SERVICE at area X with reliability guarantee Y(t)//</color>
end note
!endif
->Publish to DSP catalogue;
!endif


' ================================================================================
!ifdef STEP_A
:<color:red>A</color>3. **VERTICAL request**
----
Request a SERVICE instance with
given reliability QoE requirements
;

!ifdef SHOW_DATA
note right
	//**SERVICE_request**(location, time, reliability score)//
	----
	Geography (covered locations): //geo_1, geo_2, ..., geo_m//
	Times: //date range//
	Reliability: //r//
end note
!endif
!ifdef SHOW_ASSUMPTIONS
note left
	**Assumptions**
	----
	* Can understand DSP offer
	* Can provide all necessary information to allow DSP to fill-in SERVICE template
	** Area of operation
	** Desired QoE
	** etc.
end note
!endif
->Send request;
!endif


' ================================================================================
!ifdef STEP_A
:<color:red>A</color>4. **Create SLICE**
----
DSP Creates an end-to-end SLICE with desired QoE
;

!ifdef SHOW_DATA
note right
	//**SLICE_template**(location, time, reliability score)//
	----
	Geography (covered locations): //geo_1, geo_2, ..., geo_m//
	Times: //date range//
	Reliability: //r//
end note
!endif
!ifdef SHOW_ASSUMPTIONS
note left
	**Assumptions**
	----
	* DSP can plan a SLICE that provides end-to-end SERVICE with desired QoE
	** Knows how to "translate" the end-to-end requirements into
		an optimal selection of NSSs and their configuration
	** Should be allowed to choose NSSs from differents NSPs (domains) 
	* DSP installs policies as part of SLICE creation
	!ifdef STEP_B
	** E.g., periodically trigger action of SLICE recalculation
	!endif
	!ifdef STEP_C
	** E.g., trigger recalculation if monitoring suggests policy is violated
	* DSP installs monitors (QoE sensors) as part of SLICE creation
	** Collect enough QoS/QoE data to allow triggering action
		if needed to achieve end-to-end guarantees
	!endif
end note
!endif
-[dotted]->[Slice is running];
!endif


' ================================================================================
!ifdef STEP_D
:<color:red>D</color>5. **VERTICAL feedback**
----
VERTICAL provides (QoE) monitoring that
may trigger need to re-optimize SLICE
;
!ifdef SHOW_DATA
note right
	//**Report**(QoE)//
	----
	QoE as perceived by VERTICAL end-to-end
end note
!endif
!ifdef SHOW_ASSUMPTIONS
note left
	**Assumptions**
	----
	* Vertical is willing to share QoE metrics with DSP
	** Can collect QoE metrics and report them
	* DSP Can understand VERTICAL feednack
	** Is sysntax part of SERVICE offering?
	* Alternatively: <color:blue>//I am not getting my promised SLA//</color>
	** Only provides Yes/No or Rating 
end note
!endif
->Provide feedback;
!endif


' ================================================================================
!ifdef STEP_C
:<color:red>C</color>6. **Monitoring triggers Action**
----
(QoE) monitoring detects need to re-optimize SLICE
;
!ifdef SHOW_DATA
note right
	//**Create_monitors**(params)//
	//**Alert**(subscribers)//
	//**Actuate_Policy**()//
	----
	Create monitors that aggregate relevant QoS
	Trigger alert when aggregate exceed threshold
end note
!endif
!ifdef SHOW_ASSUMPTIONS
note left
	**Assumptions**
	----
	* Monitoring is deployed with SLICE
	** Selected from catlogue -- known filters, known aggregations
	** Configured for specific SLICE with specific aggregation
	* Monitoring can provide enough information to detect degradation in reliability
	** E.g., apply model on sequence of monitored alarms
	** E.g., infer reliability from measured latencies
	* Policies are deployed with SLICE
	** Selected from catlogue -- known rules, known actions
	** Configured for specific SLICE with specific thresholds, action parameters
end note
!endif
->Trigger alert;
!endif


' ================================================================================
!ifdef STEP_E
:<color:red>E</color>7. **VERTICAL notified**
----
DSP notifies VERTICAL about SLICE QoE
----
In response to monitoring
;
!ifdef SHOW_DATA
note right
	//**Alert**(text)//
	----
	Notify of imminenet reliability violation
	Notify of temporary SERVICE degradation during SLICE modification
end note
!endif
!ifdef SHOW_ASSUMPTIONS
note left
	**Assumptions**
	----
	* DSP is willing to share some reliability information with VERTICAL
	** Namely, report expected SLA violation
	** At least inform VERTICAL of SLICE being corrected and temporary degrdation during fix
	* Notification is useful for vertical
	** E.g., can attribute errors (as seen by VERTICAL) to SLICE vs. application
end note
!endif
->Send notification;
!endif


' ================================================================================
!ifdef STEP_B
:<color:red>B</color>8. **SLICE modified**
----
DSP modifies SLICE
----
In response to scheduled policy
!ifdef STEP_C
In reponse to monitoring
!endif
!ifdef STEP_D
In reponse to VERTICAL feedback
!endif
;
!ifdef SHOW_DATA
note right
	//**SLICE_template_update**(location, time, reliability score)//
	----
	Geography (covered locations): //geo_1, geo_2, ..., geo_m//
	Times: //date range//
	Reliability: //r//
end note
!endif
!ifdef SHOW_ASSUMPTIONS
note left
	**Assumptions**
	----
	* DSP can modify SLICE in a (almost) transparent manner
	** Can plan change
	** NSP can apply change while SLICE is running
	* DSP can replace NSS(s) of a SLICE in a (almost) transparent manner
	** NSP can transparently take over from another NSP 
end note
!endif
->Orchestrate change;
!endif

' ====END============================================================================
stop

@enduml
