(http://www.plantuml.com/plantuml/proxy?nocache&src=https://gitlab.com/slicenet/wp5/raw/master/UML/smartGrid1/smartGrid.txt?private_token=-g-tSy8Kwyu1NCRh9sZ4&fmt=svg)[no]
[yes](http://www.plantuml.com/plantuml/proxy?nocache&src=https://gitlab.com/slicenet/wp5/raw/master/UML/smartGrid1/smartGrid.txt?private_token=-g-tSy8Kwyu1NCRh9sZ4&fmt=svg)
![maybe](http://www.plantuml.com/plantuml/proxy?nocache&src=https://gitlab.com/slicenet/wp5/raw/master/UML/smartGrid1/smartGrid.txt?private_token=-g-tSy8Kwyu1NCRh9sZ4&fmt=svg)



# [another](http://www.plantuml.com/plantuml/proxy?nocache&fmt=svg&src=https://gitlab.com/api/v4/projects/9380128/repository/files/UML%2FsmartGrid1%2Fseq_create_slice.iuml/raw?ref=master&private_token=-g-tSy8Kwyu1NCRh9sZ4)

[think](http://www.plantuml.com/plantuml/png/JSvFJy9040NmUpz5ESYjJ32Og0cHn80n82R-kNWXutPAHxUxwyoKfjzUsXF7TtZl_LPgADOsFfkKL76WVFEyM-UVBxkhwuqsAFOaNCw7G7y79uH61_NiQCQNhO_NxNgV5yV2tllRRTUty_tZMtsdNujimi7vjgHMV5wR9Rq7EB9v_9wvs00cXjC2aiGVSgOmZWUoysMNRa0eHMMBqaF5dXG67mXs3qBLia4racaIFWqmYxyKbgDb6cBXqDMKhIYKP-e_)

[tada](http://www.plantuml.com/plantuml/svg/JSvFJy9040NmUpz5ESYjJ32Og0cHn80n82R-kNWXutPAHxUxwyoKfjzUsXF7TtZl_LPgADOsFfkKL76WVFEyM-UVBxkhwuqsAFOaNCw7G7y79uH61_NiQCQNhO_NxNgV5yV2tllRRTUty_tZMtsdNujimi7vjgHMV5wR9Rq7EB9v_9wvs00cXjC2aiGVSgOmZWUoysMNRa0eHMMBqaF5dXG67mXs3qBLia4racaIFWqmYxyKbgDb6cBXqDMKhIYKP-e_)

[moo](https://g.gravizo.com/source/svg?https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2Fslicenet%252Fwp5%2Frepository%2Ffiles%2FUML%252FsmartGrid1%252Fseq_create_slice.iuml%2Fraw%3Fref%3Dmaster%26private_token%3D-g-tSy8Kwyu1NCRh9sZ4%26no-cache)

[ha?](https://g.gravizo.com/svg?
@startuml

actor User
participant "First Class" as A
participant "Second Class" as B
participant "Last Class" as C

User -> A: DoWork
activate A

A -> B: Create Request
activate B

B -> C: DoWork
activate C

C --> B: WorkDone
destroy C

B --> A: Request Created
deactivate B

A --> User: Done
deactivate A

@enduml
            
')